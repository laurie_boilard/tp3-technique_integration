/*
 * Validation du champ courriel avec la longueur et la valeur
 *
 * Pour faire de la validation c'est quelques etapes (toujours les meme)
 * 1 Je vais chercher la valeur de mon champ
 * 2 Je la prend et la compare avec ce que je veux valider ; longueur, format, etc.
 * 3 Je renvois le resultat selon la comparaison
 * 
 * entre ces étapes c'est la que peut venir une certaine 
 * complexité. 
 * Ca va dépendre de vous rendu la . 
 * Ne chercher pas a aller trop loin, commencer par aller...
 *  
 */

let info_formulaire = [];
let error = [];
const form = document.getElementById("form");
// ETAPE 1
const nom = document.getElementById("nom");
const prenom = document.getElementById("prenom");
const email = document.getElementById("courriel");
const telephone = document.getElementById("telephone");
const comment = document.getElementById("commentaire");

// Outils de comparaison
const emailTest = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
const telephoneTest = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/

// Div avec les erreur a afficher
const errorContainer = document.querySelector(".error__list");

//Ensemble de comparaison pour les différents champs de texte
const chaine_a_valider = [nom, prenom];



form.addEventListener("submit",function(e){
    validateStringInput();
    validateEmailInput();
    validateTextArea();
    validateTelephoneInput();


    if (error.length > 0 ){
        errorContainerManager();
        e.preventDefault();
    }

});

function validateStringInput(){

   for(let i=0; i < chaine_a_valider.length; i++){
    let chaine = chaine_a_valider[i];

    if (chaine.value.length <= 0){
        error.push(`Le champ ${chaine.id} doit etre rempli`);
        errorCaller(chaine);
    }
}
info_formulaire.push(nom)
info_formulaire.push(prenom)
info_formulaire.push(comment)
}

// Change la classe des champs si il sont une erreur
function errorCaller(champ){
champ.classList.add("input__field--error");
}


function validateEmailInput(){
if(email.value.length <= 0 ){
    error.push(`Le champ ${email.id} doit etre rempli`);
    errorCaller(email);
}

if(!emailTest.test(email.value)){
    error.push(`Le ${email.id} n'est pas d'un format valide`);
    errorCaller(email);
}
info_formulaire.push(email)
}

function validateTelephoneInput(){

    if(telephone.value.length <= 0 ){
        error.push(`Le champ ${telephone.id} doit etre rempli`);
        errorCaller(telephone);
    }

    if(!telephoneTest.test(telephone.value)){

        error.push(`Le ${telephone.id} n'est pas d'un format valide`);
        errorCaller(telephone);
    }
    info_formulaire.push(telephone)
}

function validateTextArea(){
let spaceless = comment.value.replace(/\s/g, '');
/**
 * Etape 2
 */
if(spaceless.length <= 0){
    /**
    * Etape 3
    */
    error.push(`Le ${comment.id} n'est pas d'un format valide`);
    errorCaller(comment);
}
/**
 * Etape 2
 */
if (spaceless.length <= 20) {
        /**
        * Etape 3
        */
    error.push(`Le ${comment.id} doit contenir plus de 20 caracteres`);
    errorCaller(comment);
}
}

function errorContainerManager(){
for (err in error){
    let tag = document.createElement("p");
    let error_text = document.createTextNode(error[err]);
    tag.appendChild(error_text);
    errorContainer.appendChild(tag);
    errorContainer.classList.add("error__list--visible");
}
}